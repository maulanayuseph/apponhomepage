import React from 'react';
import {connect} from "react-redux";
import Routes from './components/Routes';
import { Helmet } from 'react-helmet';


class Main extends React.Component {
  render(){
    const {authData:{isLoggedInPemodal}} = this.props;

    return (
      // <html>
      //   <header>
      //     <Helmet>
      //       <title>AppOn</title>
      //     </Helmet>
      //   </header>
      //   <body>
          <Routes isLoggedInPemodal={isLoggedInPemodal} />
      //   </body>
      // </html>
    );
  }
}


function mapStateToProps(state) {
  return { authData: state.authReducer.authData }
}

export default connect(mapStateToProps, null)(Main)
