import React,{useState}  from "react";
import "../App.scss";
import "../AppMobile.scss";
import { Navbar, Nav, NavDropdown,Modal } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import $ from 'jquery';
import {
    Button,
    Form,
    Checkbox,
    Grid,
    Header as HeaderSemantic,
    Icon,
    Image,
    Menu,
    Segment,
    Sidebar,
    Input,
    Label,
    Dropdown,
} from 'semantic-ui-react'

function Header() {
  $(document).ready(function(){
    $('#toggleHed').click(function(event){
        event.stopPropagation();
        $(".navbar-collapse").show();
    });
    $("#toggleHed").on("click", function (event) {
        event.stopPropagation();
    });
});
  $(document).on("click", function () {
      $(".navbar-collapse").hide();
  });
  $('.navbar-collapse').click(function(event){
      event.stopPropagation();
  });

  return (
    <Router>
    
    <Navbar
      collapseOnSelect
      expand="lg"
      variant="dark"
      fixed="top"
      className="navbar"
    >
      <div className="container">
       <a href="/">
       <img
          src={require('../assets/img/logoweb.svg')}
          style={{width:"135.18px"}}
          alt=""
        />
        </a>
        <div className="row ml-auto">
          <Navbar.Toggle id="toggleHed" aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav" className="mr-auto">
            <Nav className="">
              <Nav.Link href="/" className="nav-link mx-2 navhome" id="nav">
                HOME
              </Nav.Link>
              <Nav.Link href="/" className="nav-link mx-2 navtentangkami" id="nav">
                TENTANG KAMI
              </Nav.Link>
              
              <Dropdown text='LAYANAN' className="mx-2 navlayanan nav-link" simple style={{marginTop:'7px'}} >
                <Dropdown.Menu>
                  <Dropdown.Item className="hrefLayanan" text='TOKO ONLINE' href="/Onstore" />
                  <Dropdown.Item text='ADMIN ONLINE'/>
                  <Dropdown.Item text='DESAIN PRODUK'/>
                  <Dropdown.Item text='DESAIN KEMASAN'/>
                  <Dropdown.Item text='DESAIN KONTEN'/>
                  <Dropdown.Item text='FOTOGRAFER'/>
                  <Dropdown.Item text='MARKETER'/>
                </Dropdown.Menu>
              </Dropdown>

              {/* <NavDropdown
                title="LAYANAN"
                id="collasible-nav-dropdown"
                className="mx-2 navlayanan"
              >
                <NavDropdown.Item href="Panduan">TOKO ONLINE</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">ADMIN ONLINE</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">DESAIN PRODUK</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">DESAIN KEMASAN</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">DESAIN KONTEN</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">FOTOGRAFER</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">MARKETER</NavDropdown.Item>
              </NavDropdown> */}

              <Nav.Link href="/" className="nav-link mx-2 navberita" id="nav">
                BERITA
              </Nav.Link>
              <Nav.Link href="/" className="nav-link mx-2 navkontak" id="nav">
                KONTAK
              </Nav.Link>
                
              <Nav.Link href="/" className="nav-link mx-2 navlogout" id="nav">
                KELUAR <Icon name='sign-out'/>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </div>
      </div>
    </Navbar>

    </Router>
  );
}

export default Header;