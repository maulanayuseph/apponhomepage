import React,{useState}  from "react";
import "../App.scss";
import { Navbar, Nav, NavDropdown, Button,Modal,Collapse } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import $ from 'jquery'
import { Dropdown, Menu } from 'semantic-ui-react'

function Header() {
  $(document).ready(function(){
    $('#toggleHed').click(function(event){
        event.stopPropagation();
        $(".navbar-collapse").show();
    });
    $("#toggleHed").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $(".navbar-collapse").hide();
});
$('.navbar-collapse').click(function(event){
    event.stopPropagation();
});

  return (
    <Router>
    
    <Navbar
      collapseOnSelect
      expand="lg"
      variant="dark"
      fixed="top"
      className="navbar"
    >
      <div className="container">
      <a className="monstoresub" href="/">
       <img
          src={require('../assets/img/home/html/onstore/logowebputih.svg')}
          style={{width:"135.18px"}}
          alt=""
        /></a>
        <div className="row ml-auto">
          <Navbar.Toggle id="toggleHed" aria-controls="responsive-navbar-nav" />

          <Navbar.Collapse id="responsive-navbar-nav" className="mr-auto">
            <Nav className="">
              <Nav.Link href="/" className="nav-link mx-2 navhomestore" id="nav">
                HOME
              </Nav.Link>
              <Nav.Link href="/" className="nav-link mx-2 navtentangkamistore" id="nav">
                TENTANG KAMI
              </Nav.Link>
              
              <Dropdown text='LAYANAN' className="mx-2 navlayananstore nav-link" simple style={{marginTop:'7px'}} >
                <Dropdown.Menu>
                  <Dropdown.Item className="hrefLayanan" text='TOKO ONLINE' href="/Onstore" />
                  <Dropdown.Item text='ADMIN ONLINE'/>
                  <Dropdown.Item text='DESAIN PRODUK'/>
                  <Dropdown.Item text='DESAIN KEMASAN'/>
                  <Dropdown.Item text='DESAIN KONTEN'/>
                  <Dropdown.Item text='FOTOGRAFER'/>
                  <Dropdown.Item text='MARKETER'/>
                </Dropdown.Menu>
              </Dropdown>
              
              {/* <NavDropdown
                title="LAYANAN"
                id="collasible-nav-dropdown"
                className="mx-2 navlayanan"
              >
                <NavDropdown.Item href="Panduan">TOKO ONLINE</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">ADMIN ONLINE</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">DESAIN PRODUK</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">DESAIN KEMASAN</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">DESAIN KONTEN</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">FOTOGRAFER</NavDropdown.Item>
                <NavDropdown.Item href="Simulasi">MARKETER</NavDropdown.Item>
              </NavDropdown> */}
              
              <Nav.Link href="/" className="nav-link mx-2 navberitastore" id="nav">
                KONTAK
              </Nav.Link>
              <Nav.Link href="/" className="nav-link mx-2 navkontakstore" id="nav">
                FAQ
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </div>
      </div>
    </Navbar>

    </Router>
  );
}

export default Header;
