
import React, { Component } from 'react';
import "../App.scss";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import $ from 'jquery'

import { Divider, Image, Icon } from 'semantic-ui-react'

function Footer() {
  $(window).scroll(function(){if($(this).scrollTop()>=50){$('#return-to-top').fadeIn(200);}else{$('#return-to-top').fadeOut(200);}});$('#return-to-top').click(function(){$('body,html').animate({scrollTop:0},500);});
  return (
    <div className='footer'>
       <a href="#" onClick={(e) => {
        e.preventDefault()
      }} id="return-to-top">
        <i className="big icons">
          <i aria-hidden="true" className="circle outline big icon" style={{color:'rgba(255,255,255,1)',opacity:'0.5'}}></i>
          <i aria-hidden="true" className="arrow up red icon" style={{left:'48% !important'}}></i>
        </i>
      </a>
    <div className="container">
        <div className="row row-terakhir">
          <div className="col-md-6 foot1 ">
              <p><i aria-hidden="true" className="phone white icon"></i> (022) 54425370</p>
              <p><i aria-hidden="true" className="map marker alternate white icon"></i> Jl. Asia Afrika no.133 Bandung</p>
              <p><i aria-hidden="true" className="clock outline white icon"></i> Mon - Sat 8.00 - 18.00. Sunday CLOSED</p>
          </div>
          <div className="col-md-6 foot2">
            <p><a href="#" className='btnIcon' >Kebijakan & Privasi</a></p>
            <p><a href="#" className='btnIcon' >Syarat dan Ketentuan</a></p>
            <p><a href="#" className='btnIcon' >FAQ</a></p>
          </div>
        </div>
      </div>
      <div className="container-fluid mt-4">
        <div className="row justify-content-center align-items-center pt-2 pb-2">
          <div className="col-auto text-gray-500 font-weight-light">
            <p className='foot3'>
              <a href="#" className='btnIcon' ><i aria-hidden="true" className="instagram icon big"></i></a>
              <a href="#" className='btnIcon' ><i aria-hidden="true" className="facebook official  big icon"></i></a>
              <a href="#" className='btnIcon' ><i aria-hidden="true" className="twitter  icon big"></i></a>
              <a href="#" className='btnIcon' ><i aria-hidden="true" className="linkedin square  icon big"></i> </a>
            </p>
          </div>
        </div>
      </div>
      <div className='bgfooter'></div>
      <p className='foot4'>
       Copyright AppOn 2020
      </p>
    </div>
  );
}



export default Footer;
