import React from 'react';
import { connect } from "react-redux";
import { compose } from "redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Home from '../pages/Dashboard/Home';
import Signin from '../pages/Dashboard/Signin';
import Signup from '../pages/Dashboard/Signup';
import FormKebutuhanPekerja from '../pages/Dashboard/FormKebutuhanPekerja'
import FormKebutuhanPengusaha from '../pages/Dashboard/FormKebutuhanPengusaha'
import HomeOnstore from '../pages/Dashboard/HomeOnstore'
import Pilihsubdomain from '../pages/Dashboard/Pilihsubdomain'
import Isiform from '../pages/Dashboard/IsiForm'
import Referral from '../pages/Dashboard/Referral'
import MetodePembayaran from '../pages/Dashboard/MetodePembayaran'

function PrivateRoute({ component: Component, authed, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => authed === true
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/Signin', state: { from: props.location } }} />}
    />
  )
}

class Routes extends React.Component {
  constructor(props) {
    super(props);
    this.props=props;
  }

  render(){
    return (
      <Router >
        <Route exact path='/' component={Home}/>
        <Route path='/Signin' component={Signin}/>
        <Route path='/Signup' component={Signup}/>
        <Route path='/FormKebutuhanPekerja' component={FormKebutuhanPekerja} />
        <Route path='/FormKebutuhanPengusaha' component={FormKebutuhanPengusaha} />
        <Route path='/Onstore' component={HomeOnstore} />
        <Route path='/Subdomain' component={Pilihsubdomain} />
        <Route path='/Isiform' component={Isiform} />
        <Route path='/Referral' component={Referral} />
        <Route path='/MetodePembayaran' component={MetodePembayaran} />
        { /* <Route path='/detail' component={Detail}/>
        <Route path='/home/selesai' component={Selesai}/>

        <PrivateRoute authed={this.props.auth.isLoggedInPengelola} path='/Userpengelola' component={UserPengelola} />
        <PrivateRoute authed={this.props.auth.isLoggedInPemodal} path='/Userpemodal' component={UserPemodal} /> */}
      </Router >
    );
  }

}

function mapStateToProps(state) {
  return { auth: state.authReducer.authData }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}


export default connect(mapStateToProps, mapDispatchToProps)(Routes);