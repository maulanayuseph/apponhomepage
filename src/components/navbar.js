import React,{useState}  from "react";
import "../App.scss";
import "../AppMobile.scss";
import { Navbar, Nav, NavDropdown, Button,Modal } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import $ from 'jquery'
import { Dropdown, Menu } from 'semantic-ui-react'

function Header() {
  $(document).ready(function(){
    $('#toggleHed').click(function(event){
        event.stopPropagation();
        $(".navbar-collapse").show();
    });
    $("#toggleHed").on("click", function (event) {
        event.stopPropagation();
    });
  });
  $(document).on("click", function () {
      $(".navbar-collapse").hide();
  });
  $('.navbar-collapse').click(function(event){
    event.stopPropagation();
  });

  const pathname = window.location.pathname

  return (
    <Router>
    
    <Navbar
      collapseOnSelect
      expand="lg"
      variant="dark"
      fixed="top"
      className="navbar"
    >
      <div className="container">
       <a href="/">
       <img
          src={require('../assets/img/logoweb.svg')}
          style={{width:"135.18px"}}
          alt=""
        />
        </a>
        <div className="row ml-auto">
          <Navbar.Toggle id="toggleHed"  aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav" className="mr-auto">
            <Nav className="navSign">
              <Nav.Link href="/" className="nav-link mx-2 navhome" id="nav">
                HOME
              </Nav.Link>
              <Nav.Link href="/" className="nav-link mx-2 navtentangkami" id="nav">
                TENTANG KAMI
              </Nav.Link>
              
              <Dropdown text='LAYANAN' className="mx-2 navlayanan nav-link" simple style={{marginTop:'7px'}} >
                <Dropdown.Menu>
                  <Dropdown.Item className="hrefLayanan" text='TOKO ONLINE' href="/Onstore" />
                  <Dropdown.Item text='ADMIN ONLINE'/>
                  <Dropdown.Item text='DESAIN PRODUK'/>
                  <Dropdown.Item text='DESAIN KEMASAN'/>
                  <Dropdown.Item text='DESAIN KONTEN'/>
                  <Dropdown.Item text='FOTOGRAFER'/>
                  <Dropdown.Item text='MARKETER'/>
                </Dropdown.Menu>
              </Dropdown>

              <Nav.Link href="/" className="nav-link mx-2 navberita" id="nav">
                BERITA
              </Nav.Link>
              <Nav.Link href="/" className="nav-link mx-2 navkontak" id="nav">
                KONTAK
              </Nav.Link>
              
              { (pathname == '/Signin') ? <Daftar /> : <Nav.Link href="/Signin" className="nav-link mx-2 mnavmasuk" id="nav">
                MASUK
              </Nav.Link> }

            </Nav>
          </Navbar.Collapse>
        </div>
      </div>
    </Navbar>

    </Router>
  );
}

function Daftar() {
  const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

  return (
    <>
      <Nav.Link href="#"  onClick={handleShow} className="nav-link mx-2 navdaftar" id="nav">
        DAFTAR
      </Nav.Link>
      <Modal centered show={show} onHide={handleClose} animation={false}>
          <Modal.Title className="titleModalDaftar">Daftar sebagai</Modal.Title>
       
          <div className="row text-center contentModalDaftar">
            <div className="col-md-6">
              <a href="/Signup?as=Pengusaha" className="btn my-4 btnDaftarPengusaha">PENGUSAHA</a>
            </div>
            <div className="col-md-6">
            <a href="/Signup?as=Pekerja" className="btn my-4 btnDaftarPekerja">PEKERJA</a>
            </div>
          </div>
      
      </Modal>
    </>
  );
}

export default Header;
