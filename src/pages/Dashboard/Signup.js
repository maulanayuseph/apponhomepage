import PropTypes from 'prop-types'
import React, { Component, memo, useState  } from 'react'
import {Modal} from 'react-bootstrap';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import '../../App.scss'
import Navbar from "../../components/navbar";
import googleicon from "../../assets/img/home/html/googleicon.svg"
import fbicon from "../../assets/img/home/html/facebookicon.svg"
import terkirimpic from "../../assets/img/home/html/terkirimpic.svg"
import {
  Button,
  Form,
  Checkbox,
  Grid,
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Input,
  Label,
} from 'semantic-ui-react'

export default class Signup extends Component {
  state = {
    animation: 'push',
    as: 'Pengusaha',
  }
  
  findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
  }
  
  componentDidMount() {
      var cek = this.findGetParameter('as');

      if (cek === 'Pengusaha' || cek === 'Pekerja') {
        this.setState({
            as: this.findGetParameter('as'),
        });
      } else {
        window.location.href = '/'
      }

    }

  render() {
    return (
        <div className='bgheadSignin'>
          <Navbar/>
          <FormSignup as={this.state.as}/>
      </div>
    )
  }

}


const FormSignup = memo((props) => {

    const [show, setShow] = useState(false);
  
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
      <Container style={{paddingBottom:'50px'}}>
      <Row className="justify-content-md-center">
          <Col xs="12" md="6" className='headtopp3'>
            <div className='detailheadtoppSign'>
            <h4>Daftar Sebagai {props.as}</h4>

            <Form>
              <Form.Field>
                <label>Nama</label>
                <input placeholder='Nama' />
              </Form.Field>
              <Form.Field>
                <label>Email</label>
                <input placeholder='Email' />
              </Form.Field>
              <Form.Field>
                <label>Password</label>
                <input type='password' placeholder='Password' />
              </Form.Field>
              <Form.Field>
                <label>Ulangi Password</label>
                <input type='password' placeholder='Ulangi Password' />
              </Form.Field>
              <div className="field">
                  <div className="ui checkbox">
                    <input type="checkbox" />
                    <label>
                      Dengan ini saya menyetujui syarat & ketentuan 
                      <strong>
                      <a href="#" style={{textDecoration:'none',color:'black'}} onClick={(e) => { e.preventDefault(); }}> AppOn</a> 
                      </strong>
                    </label>
                  </div>
              </div>
              <Form.Field className='row'>
                <Button type='submit' className='btn daftarBtnSignin colorSign' onClick={handleShow}>DAFTAR</Button>
              </Form.Field>
            </Form>
            </div>
            <div className='subheadtoppSign'>
              Atau masuk dengan
              <Image.Group size='tiny'>
                  <a href="#" onClick={(e) => {
                    e.preventDefault()
                  }} style={{paddingRight:'30px',textDecoration:'none'}}>
                    {/* <i aria-hidden="true" className="google red big icon"></i> */}
                    <Image src={googleicon} />
                  </a>
                  <a href="#" onClick={(e) => {
                    e.preventDefault()
                  }}>
                    <Image src={fbicon} />
                    {/* <i aria-hidden="true" className="facebook official red big icon"></i> */}
                  </a>
                </Image.Group>
            </div>
          </Col>
          <Col xs="12" md="6" className='paddingDaftar'>
            <div className='textSigninDaftar'>
                Anda sudah mempunyai akun?
            </div>
            <a href="/Signin" className="btn daftarBtnSignup">MASUK</a>
          </Col>
      </Row>
      <Modal centered show={show} onHide={handleClose} animation={false}>
      <Modal.Title className="titleModalDaftarEmail">Berhasil !</Modal.Title>
      <div className='subtitleModalDaftarEmail'>
          Harap cek email anda
      </div>

      <div className="row text-center justify-content-md-center contentModalDaftar2">
          <Image src={terkirimpic} />
          <div className="col-md-12">
              { (props.as == 'Pekerja') ? 
                (<a href="/FormKebutuhanPekerja" className="btn my-4 btnDaftarMasuk">MASUK</a>) 
                : 
                (<a href="/FormKebutuhanPengusaha" className="btn my-4 btnDaftarMasuk">MASUK</a>) 
              }
            </div>
            <div className="col-md-12 divbtnDaftarKembali">
            <a href="/" className="btn btnDaftarKembali">KEMBALI KE BERANDA</a>
          </div>
      </div>

      </Modal>

      </Container>
    )
})