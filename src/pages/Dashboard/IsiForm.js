import PropTypes from 'prop-types'
import React, { Component, memo, useState  } from 'react'
import {Modal} from 'react-bootstrap';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import '../../App.scss'
import '../../AppMobile.scss'
import Navbar from "../../components/navbar_onstoresub";
import googleicon from "../../assets/img/home/html/googleicon.svg"
import fbicon from "../../assets/img/home/html/facebookicon.svg"
import bgtesti from '../../assets/img/home/html/onstore/Path 647.svg';
import nomor1 from '../../assets/img/home/html/onstore/Group 15268 line.png';
import nomor2 from '../../assets/img/home/html/onstore/Group 15266.png';
import nomor3 from '../../assets/img/home/html/onstore/Group 15267 line.png';
import nomor4 from '../../assets/img/home/html/onstore/Group 15269 line.png';
import {
  Button,
  Form,
  Checkbox,
  Grid,
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Input,
  Label,
} from 'semantic-ui-react'


export default class IsiForm extends Component {
  state = {
    animation: 'push',
  }
  
  render() {
    return (
        <div className='bgheadstore subdomain'>
          <Navbar/>
          <HeaderKonten/>
      </div>
    )
  }
  
}

function HeaderKonten() {
    return (
        <Container>
        <Row className="justify-content-md-left">
            <Col xs="12" md="6" className='headtoppsubdomain isiform'>
            <Header as='h2' className="isiform mlangkah1">
                    <Image src={nomor1}/>
                    <Header.Content>
                    <span className="headstep">Langkah 1</span>
                    <Header.Subheader><span className="subheadstep">Pilih Nama Toko</span></Header.Subheader>
                </Header.Content>
            </Header>

            <Header as='h2' className="paddDomain isiform mlangkah2">
                    <Image src={nomor2} className=""/>
                    <Header.Content>
                    <span className="headstepnotactive">Langkah 2</span>
                    <Header.Subheader><span className="subheadstepnotactive">Isi data diri</span></Header.Subheader>
                </Header.Content>
            </Header>

            <Header as='h2' className="paddDomain isiform mlangkah3">
                    <Image src={nomor3} className="notactive"/>
                    <Header.Content>
                    <span className="headstepnotactive notactive">Langkah 3</span>
                    <Header.Subheader><span className="subheadstepnotactive notactive">Masukan kode promosi</span></Header.Subheader>
                </Header.Content>
            </Header>

            <Header as='h2' className="paddDomain isiform mlangkah3">
                    <Image src={nomor4} className="notactive"/>
                    <Header.Content>
                    <span className="headstepnotactive notactive">Langkah 4</span>
                    <Header.Subheader><span className="subheadstepnotactive notactive">Metode pembayaran</span></Header.Subheader>
                </Header.Content>
            </Header>

            </Col>
            <Col xs="12" md="6" className='isiform formheadtoppsubdomain'>
                <div className="formNamaToko">
                <Form>
                    <Form.Field>
                        <label>Nama</label>
                        <input placeholder='Nama' />
                    </Form.Field>
                    <Form.Field>
                        <label>Email</label>
                        <input placeholder='Email' />
                    </Form.Field>
                    <Form.Field>
                        <label>Nomor Handphone</label>
                        <input placeholder='Nomor Handphone' />
                    </Form.Field>
                    <Form.Field className="footerBtn">
                        <Button type='button' className="isiform btnNextToko Kembali" href="/Subdomain">
                            <Icon name='left arrow' className="warnaRed" />
                            Kembali
                        </Button>
                        <Button type='button' className="isiform btnNextToko" href="/Referral">
                            Selanjutnya
                            <Icon name='right arrow' className="warnaRed" />
                        </Button>
                    </Form.Field>
                </Form>
                </div>
                <div className="footerNext2">
                    <span className="textchooseLogin">Anda sudah mempunyai akun?</span> <a href="/Signin" className="btnchooseLogin">Masuk disini</a>
                </div>
            </Col>
        </Row>
        </Container>
    )
}
