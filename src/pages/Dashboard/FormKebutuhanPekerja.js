import PropTypes from 'prop-types'
import React, { Component, memo, useState  } from 'react'
import {Modal} from 'react-bootstrap';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import '../../App.scss'
import '../../AppMobile.scss'
import Navbar from "../../components/navbar_dashboard";
import googleicon from "../../assets/img/home/html/googleicon.svg"
import fbicon from "../../assets/img/home/html/facebookicon.svg"
import delIcon from "../../assets/img/home/html/Mask_Group_224.png"
import bgilustrasi from "../../assets/img/home/bgilustrasi.svg"

import $ from "jquery"
import {
  Button,
  Form,Dropdown,
  Checkbox,
  Grid,
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Input,
  TextArea,
  Label,
} from 'semantic-ui-react'


export default class FormKebutuhanPekerja extends Component {
  state = {
    animation: 'push',
    activeItem:'PROFILE',
    users:[],
  }
  

  render() {
    const handleItemClick = (e, { name }) => {
        this.setState({ activeItem: name })
        if (name === 'PROFILE') {
            $("#PROFILE").attr('style','display');
        } else {
            $("#PROFILE").attr('style','display:none');
        }
    }

    const addUser = (e) => {
        this.setState({
            users: [...this.state.users, <User addUser={this.state.users} />]
        })
    }

    return (
        <div className='bgheadDashboard'>
          <Navbar/>
          <HeadBar addUser={addUser} users={this.state.users} activeItem={this.state.activeItem} handleItemClick={handleItemClick}/>
      </div>
    )
  }

}

const User = memo((props) => {
    return (
        <Form.Field>
            <Input
                action={
                    <Button className='btnUploadFile' type="file" style={{color:"#FFFFFF"}}>Upload</Button>
                }
                className="addKeahlian"
                icon='attach'
                iconPosition='left'
                placeholder='File001.jpeg'
            />
            <Button className='imgdelfileKeahlian' icon>
                <Image src={delIcon} size="mini" className="delfileKeahlian"/>
            </Button>
        </Form.Field>
        )
});

const HeadBar = memo((props) => {
    return (
        <Row className="justify-content-md-center ">
            <Col xs="12" md="12">
            <Segment className='menuHeadbar' inverted>
                <Menu inverted pointing secondary>
                    <Menu.Item
                    name='PROFILE'
                    active={props.activeItem === 'PROFILE'}
                    onClick={props.handleItemClick}
                    />
                    <Menu.Item
                    name='DASHBOARD'
                    />
                    <Menu.Item
                    name='INBOX'
                    />
                </Menu>
            </Segment>
            <Konten addUser={props.addUser} users={props.users}/>
            </Col>
        </Row>
    );
});

const Konten = memo((props) => {
    const onClickPrev = (e) => {
        e.preventDefault()
        props.addUser()
    }
      
    return (
        <Container>
        <Row className="justify-content-md-center">
          <Col xs="12" md="12" className='kontenKebPekerja'>
            <Grid stackable columns={2}>
                <Grid.Row>
                <Grid.Column>
                    <span className='judulkontenKebPekerja'>Silahkan lengkapi data berikut agar kami dapat 
                    <br/>mengetahui dan membantu sesuai kebutuhan anda</span>
                    <Form className='formKebPekerja'>
                        <Form.Field>
                            <label>Nama</label>
                            <input placeholder='Nama' />
                        </Form.Field>
                        <Form.Field>
                            <label>Email</label>
                            <input placeholder='Email' />
                        </Form.Field>
                        <Form.Field>
                            <label>Password</label>
                            <input placeholder='Password' />
                        </Form.Field>
                        <Form.Field>
                            <label>Alamat</label>
                            <TextArea placeholder='Alamat' />
                        </Form.Field>
                        <Form.Field>
                            <label>Upload KTP</label>
                            <Input
                                action={
                                    <Button className='btnUploadFile' type="file" style={{color:"#FFFFFF"}}>Upload</Button>
                                }
                                icon='attach'
                                iconPosition='left'
                                placeholder='File001.jpeg'
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>Pendidikan Terakhir</label>
                            <input placeholder='SMA / SMK / D3 / S1 / S2 / S3' />
                        </Form.Field>
                        <Form.Field key="keahlian1">
                            <label>Keahlian</label>
                            <Input
                                action={
                                    <Button className='btnUploadFile' type="file" style={{color:"#FFFFFF"}}>Upload</Button>
                                }
                                icon='attach'
                                iconPosition='left'
                                placeholder='File001.jpeg'
                            />
                        </Form.Field>
                        {props.users}
                        <Form.Field>
                            <a color="black" href="#" onClick={onClickPrev} className="textTambahLampiran">Tambah Lampiran</a>
                        </Form.Field>
                        <Form.Field className='row'>
                            <a href='/FormKebutuhanPekerja' className='btn simpanKeb colorSign'>SIMPAN</a>
                        </Form.Field>
                    </Form>
                </Grid.Column>
                </Grid.Row>
            </Grid>
          </Col>
      </Row>
      </Container>
    )
})