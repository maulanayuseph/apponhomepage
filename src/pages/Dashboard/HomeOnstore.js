import React,{useState} from 'react';
import "../../App.scss";
import "../../AppMobile.scss";
import { connect } from "react-redux";
import { BallBeat } from 'react-pure-loaders';
import pengusaha from "../../assets/img/home/html/pengusahailustrasi.svg"
import pegawai from "../../assets/img/home/html/pekerjailustrasi.svg"
import berita1 from "../../assets/img/home/thought-catalog-UK78i6vK3sc-unsplash@2x.png"
import berita2 from "../../assets/img/home/glenn-carstens-peters-SL5d_8ywAAA-unsplash@2x.png"
import berita3 from "../../assets/img/home/dayne-topkin-y5_mFlLMwJk-unsplash@2x.png"
import Navbar from "../../components/navbar_onstore"
import Footer from "../../components/Footer";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Carousel from 'react-bootstrap/Carousel'
import slideimg from '../../assets/img/home/smartphone_mockup@2x.png';
import terlaris from '../../assets/img/home/html/onstore/Group 15276.svg';
import keuntungan1 from '../../assets/img/home/html/onstore/1.svg';
import keuntungan2 from '../../assets/img/home/html/onstore/2.svg';
import keuntungan3 from '../../assets/img/home/html/onstore/3.svg';
import bgtesti from '../../assets/img/home/html/onstore/Path 647.svg';
import orgtesti from '../../assets/img/home/html/onstore/maintenancepic.png';

import $ from 'jquery'

import { Divider, Image, Icon, Grid, Menu, Segment } from 'semantic-ui-react'

class HomeOnstore extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoading: true,
      data:[]
    };
  }
  
  componentDidMount() {
    this.setState({isLoading:true});
}
  
	render() {
    
    $("#beliSekarang").click(function() {
        $('html, body').animate({
            scrollTop: $("#ChooseBeli").offset().top
        }, 2000);
    });

		return(
      <div className='bgheadstore'>
          <BallBeat
          color={'#123abc'}
          loading={this.isLoading}
          />
          <Navbar/>
          <Header/>
          <Pendaftaran/>
          <Keuntungan />
          <Testimonial/>
          <Footer/>
      </div>
		);
	}
  
}

function Header() {
  return (
    <Container>
    <Row className="justify-content-md-center">
        <Col xs="12" md="6" className='headtopp store'>
          <div className='detailheadtopp'>
            Tingkatkan peluang penjualan dengan menggunakan toko online
          </div>
          <div className='subheadtopp'>
          Semakin banyak yang bisa mengakses website kamu, berarti <br/>
          semakin banyak peluang terciptanya penjualan, ubah dengan <br/> cepat kunjungan menjadi cuan!
          </div>
          <a href="#" id="beliSekarang" className="btn btn-danger beliBtn">Beli Sekarang</a>
        </Col>
        <Col xs="12" md="6" className='bgtopstore'></Col>
    </Row>
    </Container>
  );
}

function Pendaftaran() {
  return (
    <Container>
    <Row className="justify-content-md-center">
        <Col id="ChooseBeli"  xs="12" md="12" className='pendaftaranappstore'>
            <span id="ChooseBeli" className="headpendaftaranstore">
              Paket tepat untuk bisnis anda, tanpa minimal waktu berlangganan, <br/>dan dapat berhenti tanpa syarat:
            </span>
            <Grid textAlign='center' className="choosePaket" columns='three'>
              <Grid.Row>
                <Grid.Column className="col-md-4 col-12">
                  <div className="widthPaket">
                    <div className="bgtitlePaket">
                      <div className="titlePaket">
                        <div className="titlePaketPro">Toko Standard</div>
                        <span className="coretPaket">400.000/bulan</span> <span className="diskonPaket">200.000/bulan</span>
                      </div>
                      <div className="groupcontentPaket">
                        <div className="contentPaket">
                          Jumlah produk <p className="subcontentPaket">25 produk</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Kapasitas ruang simpan data <p className="subcontentPaket">500mb</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Whatsapp chat <p className="subcontentPaket">Tidak</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Penghitungan ongkos kirim <p className="subcontentPaket">Ya</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Metode pembayaran transfer bank <p className="subcontentPaket">Ya</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Metode pembayaran kartu kredit <p className="subcontentPaket">Tidak</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Metode pembayaran Paypal <p className="subcontentPaket">Tidak</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Layanan admin <p className="subcontentPaket">Pengelolaan 5 produk/bulan</p>
                          <div className="garisPaket"></div>
                        </div>
                        <a href="/Subdomain" className="btn btn-danger beliBtnPaket">Beli Sekarang</a>
                      </div>
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column className="col-md-4 col-12 paket2">
                <div className="widthPaket">
                    <div className="bgtitlePaket">
                      <Image src={terlaris} className="imgTerlaris"/>
                      <div className="titlePaket">
                        <div className="titlePaketBisnis">Toko Bisnis</div> 
                        <span className="coretPaket">500.000/bulan</span> <span className="diskonPaket">250.000/bulan</span>
                      </div>
                      <div className="groupcontentPaketBisnis">
                        <div className="contentPaket">
                          Jumlah produk <p className="subcontentPaket">50 produk</p>
                          <div className="garisPaketLaris"></div>
                        </div>
                        <div className="contentPaket">
                          Kapasitas ruang simpan data <p className="subcontentPaket">1gb</p>
                          <div className="garisPaketLaris"></div>
                        </div>
                        <div className="contentPaket">
                          Whatsapp chat <p className="subcontentPaket">Ya</p>
                          <div className="garisPaketLaris"></div>
                        </div>
                        <div className="contentPaket">
                          Penghitungan ongkos kirim <p className="subcontentPaket">Ya</p>
                          <div className="garisPaketLaris"></div>
                        </div>
                        <div className="contentPaket">
                          Metode pembayaran transfer bank <p className="subcontentPaket">Ya</p>
                          <div className="garisPaketLaris"></div>
                        </div>
                        <div className="contentPaket">
                          Metode pembayaran kartu kredit <p className="subcontentPaket">Tidak</p>
                          <div className="garisPaketLaris"></div>
                        </div>
                        <div className="contentPaket">
                          Metode pembayaran Paypal <p className="subcontentPaket">Tidak</p>
                          <div className="garisPaketLaris"></div>
                        </div>
                        <div className="contentPaket">
                          Layanan admin <p className="subcontentPaket">Pengelolaan 10 produk/bulan</p>
                          <div className="garisPaketLaris"></div>
                        </div>
                        <a href="/Subdomain"  className="btn btn-danger beliBtnPaket">Beli Sekarang</a>
                      </div>
                    </div>
                  </div>
                </Grid.Column>
                <Grid.Column className="col-md-4 col-12 paket2">
                <div className="widthPaket">
                    <div className="bgtitlePaket">
                      <div className="titlePaket">
                        <div className="titlePaketPro">Toko Pro</div> 
                        <span className="coretPaket">400.000/bulan</span> <span className="diskonPaket">200.000/bulan</span>
                      </div>
                      <div className="groupcontentPaket">
                        <div className="contentPaket">
                          Jumlah produk <p className="subcontentPaket">100 produk</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Kapasitas ruang simpan data <p className="subcontentPaket">2gb</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Whatsapp chat <p className="subcontentPaket">Ya</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Penghitungan ongkos kirim <p className="subcontentPaket">Ya</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Metode pembayaran transfer bank <p className="subcontentPaket">Ya</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Metode pembayaran kartu kredit <p className="subcontentPaket">Opsional *</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Metode pembayaran Paypal <p className="subcontentPaket">Opsional *</p>
                          <div className="garisPaket"></div>
                        </div>
                        <div className="contentPaket">
                          Layanan admin <p className="subcontentPaket">Pengelolaan 30 produk/bulan</p>
                          <div className="garisPaket"></div>
                        </div>
                        <a href="/Subdomain"  className="btn btn-danger beliBtnPaket">Beli Sekarang</a>
                      </div>
                    </div>
                  </div>
                </Grid.Column>
              </Grid.Row>
            </Grid>
        </Col>
    </Row>
    </Container>
  );
}

function Keuntungan() {
  return (
    <Container>
      <Col xs="12" md="12" className=''>
          <Row className="justify-content-md-center">
            <div className='dividerKeuntungan'></div>
          </Row>
          <Row className="justify-content-md-center">
            <div className="titleKeuntungan">Dapatkan keuntungan</div>
          </Row>
          <Row className="justify-content-md-center">
            <div className="subtitleKeuntungan">Daftarkan usaha dan toko anda di AppOn, dan tingkatkan target anda.</div>
          </Row>
          <Row className="justify-content-md-center">
            <Grid textAlign='center' className="kontenKeuntungan" columns='three'>
                <Grid.Row>
                  <Grid.Column className="col-md-4 col-12">
                      <Image src={keuntungan1} centered/>
                      <div className="judulKeuntungan">Membangun aset bisnis</div>
                      <div className="descKeuntungan">
                        Mulai bangun aset digital usaha dari toko online eksklusif-mu sendiri, untuk memudahkan konsumen membeli 
                        produk andalan kamu. Kontrol penuh pengunjung baru dan pelanggan lama karena mereka tidak teralihkan karena 
                        tampilnya produk kompetitor.
                      </div>
                  </Grid.Column>
                  <Grid.Column className="col-md-4 col-12 keuntungan2">
                    <Image src={keuntungan2} centered/>
                    <div className="judulKeuntungan">Mengelola aset bisnis</div>
                    <div className="descKeuntungan">
                    Rasakan kemudahan dalam mengelola penjualan online, sehingga anda dapat lebih efisien dan fokus pada produksi, 
                    Dapatkan bantuan dari AppOn untuk mengelola bahkan mengembangkan usahamu lebih jauh, memperluas pasar dan konsumen, 
                    sebarkan produk dengan lebih mudah melalaui jejaring digital dan bangun interaksi dengan media sosial
                    </div>
                  </Grid.Column>
                  <Grid.Column className="col-md-4 col-12 keuntungan2">
                    <Image src={keuntungan3} centered className="imgKeuntungan3"/>
                    <div className="judulKeuntungan">Edukasi & konsultasi bisnis</div>
                    <div className="descKeuntungan">
                    Tidak hanya mendapatkan toko online dengan beragam fitur, mulai dari pengelolaan produk, pesanan, mengatur pengiriman, 
                    menghitung pendapatan, reseller, bagi hasil. Namun kamu juga bisa mendapatkan akses untuk mengembangkan bisnismu di AppOn Center, 
                    dari edukasi mandiri hingga berkonsultasi dengan para ahli yang dapat membuat usahamu naik kelas!
                    </div>
                  </Grid.Column>
                </Grid.Row>
            </Grid>
          </Row>
        </Col>
    </Container>
  );
}

function Testimonial() {
  return (
    <Container>
      <Col xs="12" md="12" className='cardTestimonial'>
          <Row className="justify-content-md-center">
            <div className="titleTestimonial">Testimonials</div>
          </Row>
          <Row className="justify-content-md-center">
            <div className="subtitleTestimonial">Testimonilas yang sudah pernah memakai layanan AppOn</div>
          </Row>
          <Row className="justify-content-md-center">
          <Carousel className="testimoni">
            <Carousel.Item>
              <Row className="justify-content-md-center">
                  <Col xs="12" md="12" className=''>
                      <Row className="justify-content-md-center sliderTestimonial" >
                          <div className="divimgTesti">
                            <Image src={orgtesti} circular className="imgTesti" />
                          </div>
                          <h6 className="namaTesti">Cinta Bella</h6>
                          <h6 className="subnamaTesti">Coffe Toffe Owner</h6>
                          <div className="descTesti">
                          " Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et 
                          dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 
                          Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, 
                          consetetur sadipscing elitr "
                          </div>
                      </Row>
                  </Col>
              </Row>
            </Carousel.Item>
            <Carousel.Item>
              <Row className="justify-content-md-center">
                  <Col xs="12" md="12" className=''>
                      <Row className="justify-content-md-center sliderTestimonial" >
                          <div className="divimgTesti">
                            <Image src={orgtesti} circular className="imgTesti" />
                          </div>
                          <h6 className="namaTesti">Cinta Bella</h6>
                          <h6 className="subnamaTesti">Coffe Toffe Owner</h6>
                          <div className="descTesti">
                          " Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et 
                          dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 
                          Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, 
                          consetetur sadipscing elitr "
                          </div>
                      </Row>
                  </Col>
              </Row>
            </Carousel.Item>
            <Carousel.Item>
              <Row className="justify-content-md-center">
                  <Col xs="12" md="12" className=''>
                      <Row className="justify-content-md-center sliderTestimonial" >
                          <div className="divimgTesti">
                            <Image src={orgtesti} circular className="imgTesti" />
                          </div>
                          <h6 className="namaTesti">Cinta Bella</h6>
                          <h6 className="subnamaTesti">Coffe Toffe Owner</h6>
                          <div className="descTesti">
                          " Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et 
                          dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 
                          Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, 
                          consetetur sadipscing elitr "
                          </div>
                      </Row>
                  </Col>
              </Row>
            </Carousel.Item>
          </Carousel>
          </Row>
        </Col>
    </Container>
  );
}

function Berita() {
  return (
    <Container>
    <Row className="justify-content-md-center">
        <Col xs="12" md="12" className='beritaapp'>
            <span className="headberita">Berita seputar dunia kerja & usaha</span>
            <p className="beritatentang">Temukan berita, tips, trik dan saran dari AppOn</p>
            <Row className="justify-content-md-center mberitaRow">
              <Col xs="12" md="3" className="cardberita mberita1" style={{background:'url('+berita1+')',
              backgroundSize:'100%',backgroundRepeat:'no-repeat',
              paddingLeft:'0px',backgroundColor:'#E9E9E9'}}>
                <div className='bgjudulberita'>
                  <p className='judulberita'>Kerja Remote: Mulailah Dari Hal yang Paling Sederhana</p>
                </div>
                <Row className="justify-content-md-center midberita">
                  <Col xs="6" md="6">
                    <Icon name='user outline' />
                    Admin
                  </Col>
                  <Col xs="6" md="6">
                    <Icon name='clock outline' />
                    Kamis, 10 April 2020
                  </Col>
                </Row>
                <div className='padjudulberita'>
                  <p className='descjudulberita'>
                    Kerja Remote: Mulailah Dari Hal yang Paling Sederhana.
                    Bekerja dari jarak jauh atau mengurangi aktivitas pertemuan fisik, sebenarnya bukan hanya terjadi pada saat…
                  </p>
                </div>
                <a href="#" className="btn BtnSelengkapnya">Baca Selengkapnya</a>
              </Col>
              <Col xs="12" md="3" className="cardberita mberita2" style={{background:'url('+berita2+')',
              backgroundSize:'100%',backgroundRepeat:'no-repeat',
              paddingLeft:'0px',backgroundColor:'#E9E9E9'}}>
                <div className='bgjudulberita'>
                  <p className='judulberita'>Kerja Remote: Mulailah Dari Hal yang Paling Sederhana</p>
                </div>
                <Row className="justify-content-md-center midberita">
                  <Col xs="6" md="6">
                    <Icon name='user outline' />
                    Admin
                  </Col>
                  <Col xs="6" md="6">
                    <Icon name='clock outline' />
                    Kamis, 10 April 2020
                  </Col>
                </Row>
                <div className='padjudulberita'>
                  <p className='descjudulberita'>
                    Kerja Remote: Mulailah Dari Hal yang Paling Sederhana.
                    Bekerja dari jarak jauh atau mengurangi aktivitas pertemuan fisik, sebenarnya bukan hanya terjadi pada saat…
                  </p>
                </div>
                <a href="#" className="btn BtnSelengkapnya">Baca Selengkapnya</a>
              </Col>
              <Col xs="12" md="3" className="cardberita mberita3" style={{background:'url('+berita3+')',
              backgroundSize:'100%',backgroundRepeat:'no-repeat',
              paddingLeft:'0px',backgroundColor:'#E9E9E9'}}>
                <div className='bgjudulberita'>
                  <p className='judulberita'>Kerja Remote: Mulailah Dari Hal yang Paling Sederhana</p>
                </div>
                <Row className="justify-content-md-center midberita">
                  <Col xs="6" md="6">
                    <Icon name='user outline' />
                    Admin
                  </Col>
                  <Col xs="6" md="6">
                    <Icon name='clock outline' />
                    Kamis, 10 April 2020
                  </Col>
                </Row>
                <div className='padjudulberita'>
                  <p className='descjudulberita'>
                    Kerja Remote: Mulailah Dari Hal yang Paling Sederhana.
                    Bekerja dari jarak jauh atau mengurangi aktivitas pertemuan fisik, sebenarnya bukan hanya terjadi pada saat…
                  </p>
                </div>
                <a href="#" className="btn BtnSelengkapnya">Baca Selengkapnya</a>
              </Col>
            </Row>
            <Row className="justify-content-md-center" style={{paddingTop:'50px'}}>
              <a href="#" className="btn BtnArtikel">Lihat Semua Artikel <i aria-hidden="true" className="arrow right red icon"></i> </a>
            </Row>
        </Col>
    </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return { loginUser: state.authReducer.loginUser }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeOnstore);
