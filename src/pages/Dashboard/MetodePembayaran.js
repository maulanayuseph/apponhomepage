import PropTypes from 'prop-types'
import React, { Component, memo, useState  } from 'react'
import {Modal} from 'react-bootstrap';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import '../../App.scss'
import '../../AppMobile.scss'
import Navbar from "../../components/navbar_onstoresub";
import googleicon from "../../assets/img/home/html/googleicon.svg"
import fbicon from "../../assets/img/home/html/facebookicon.svg"
import bgtesti from '../../assets/img/home/html/onstore/Path 647.svg';
import nomor1 from '../../assets/img/home/html/onstore/Group 15268 line.png';
import nomor2 from '../../assets/img/home/html/onstore/Group 15266 line.png';
import nomor3 from '../../assets/img/home/html/onstore/Group 15267 line.png';
import nomor4 from '../../assets/img/home/html/onstore/Group 15269.png';
import {
  Button,
  Form,
  Checkbox,
  Grid,
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Input,
  Label,
} from 'semantic-ui-react'


export default class MetodePembayaran extends Component {
  state = {
    animation: 'push',
  }
  
  render() {
    return (
        <div className='bgheadstore subdomain'>
          <Navbar/>
          <HeaderKonten/>
      </div>
    )
  }
  
}

function HeaderKonten() {
    return (
        <Container>
        <Row className="justify-content-md-left">
            <Col xs="12" md="6" className='headtoppsubdomain metodebayar'>
            <Header as='h2' className="metodebayar mlangkah1">
                    <Image src={nomor1}/>
                    <Header.Content>
                    <span className="headstep">Langkah 1</span>
                    <Header.Subheader><span className="subheadstep">Pilih Nama Toko</span></Header.Subheader>
                </Header.Content>
            </Header>

            <Header as='h2' className="paddDomain metodebayar mlangkah2">
                    <Image src={nomor2} className=""/>
                    <Header.Content>
                    <span className="headstepnotactive">Langkah 2</span>
                    <Header.Subheader><span className="subheadstepnotactive">Isi data diri</span></Header.Subheader>
                </Header.Content>
            </Header>

            <Header as='h2' className="paddDomain metodebayar mlangkah3">
                    <Image src={nomor3} className=""/>
                    <Header.Content>
                    <span className="headstepnotactive ">Langkah 3</span>
                    <Header.Subheader><span className="subheadstepnotactive ">Masukan kode promosi</span></Header.Subheader>
                </Header.Content>
            </Header>

            <Header as='h2' className="paddDomain metodebayar mlangkah4">
                    <Image src={nomor4} className=""/>
                    <Header.Content>
                    <span className="headstepnotactive">Langkah 4</span>
                    <Header.Subheader><span className="subheadstepnotactive">Metode pembayaran</span></Header.Subheader>
                </Header.Content>
            </Header>

            </Col>

            <Col xs="12" md="6" className='formheadtoppsubdomain metodebayar'>
                <div className="formNamaToko">
                    <h2 className="judulPembayaran">Detail Pembayaran</h2>
                    <div className="kontenPembayaran">
                        <p>
                            Nama Toko : tokoku.appon.id
                        </p>
                        <p>
                            Nama Pemesan : Sukidi
                        </p>
                        <p>
                            Email Pemesan : sukidi@gmail.com
                        </p>
                        <p>
                            Nomor Handphone : 0818123456789
                        </p>
                        <p>
                            Kode Promosi : -
                        </p>
                        <p>
                            Pilihan Paket : Toko Standar
                        </p>
                        <p>
                            Harga Paket : Rp. 200.000 .-
                        </p>
                    </div>
                    <div className="formPembayaran">
                    <Form>
                        <Form.Field>
                            <label>Jumlah yang harus dibayarkan</label>
                            <div className="ui right action input copyBayar">
                                <input type="text" value="Rp.200.123" readOnly />
                                <button className="ui icon left basic button basic ">
                                    <i aria-hidden="true" className="copy icon"></i>
                                     Salin Jumlah
                                </button>
                            </div>
                            <p className="subnamatoko">3 dijit dibelakang adalah kode unik untuk pembayaran anda</p>
                        </Form.Field>
                    </Form>
                    </div>
                    <div className="infoTransfer">
                        <Row className="justify-content-md-left">
                            <Col className="infoTrf1">
                                <p>Info transfer :</p> 
                                PT ARMADA ENTITAS ONLINE<br/>
                                Bank Mandiri <br/>Cabang: KCP Bandung Kopo 13007
                                <br/>No Rekening: 130-05-1610000-8
                            </Col>
                            <Col xs="12" md="6" className="infoTrf2">
                                <p>&nbsp;</p>
                                PT ARMADA ENTITAS ONLINE 
                                <br/>Bank Bjb 
                                <br/>Cabang: Cibaduyut 
                                <br/>No Rekening: 00912-7416-7100
                            </Col>
                        </Row>
                    </div>
                    <div className="footerNext3 metodebayar">
                        <Button type='button' className="isiform btnNextToko Kembali" href="/Referral">
                            <Icon name='left arrow' className="warnaRed" />
                            Kembali
                        </Button>
                        <Daftar/>
                    </div>
                </div>
            </Col>

        </Row>
        </Container>
    )
}

function Daftar() {
    const [show, setShow] = useState(false);
  
      const handleClose = () => setShow(false);
      const handleShow = () => setShow(true);
  
    return (
      <>
        <Button type='submit' onClick={handleShow} className="isiform btnNextDaftar">
            Daftar
        </Button>
        <Modal centered show={show} onHide={handleClose} animation={false}>
        <Modal.Title className="DaftarDomain">
            <span className="titleDaftarDomain">Terima kasih, harap lakukan pembayaran sesuai dengan pesanan</span>
        </Modal.Title>
        <div className='subtitleDaftarDomain'>
            Transfer Melalui : <br/>
            Bank mandiri 45678411345 <br/>
            Jumlah transfer: 200.123 <br/>
            <button className="ui icon left basic button basic btncopyJumlah">
                <i aria-hidden="true" className="copy icon"></i>
                <span className="copyJumlah">&nbsp; Salin Jumlah</span>
            </button><br/>
            Batas waktu pembayaran anda :
            <div className="timeDaftarDomain">02:59:00</div>
        </div>
        <div className="row text-center justify-content-md-center contentModalDaftar3">
            <div className="col-md-12">
                <a href="/Onstore" className="btn my-4 btnDaftarMasuk">Tutup</a>
            </div>
        </div>

        </Modal>
      </>
    );
  }