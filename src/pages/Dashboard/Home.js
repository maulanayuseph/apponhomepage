import React,{useState} from 'react';
import "../../App.scss";
import "../../AppMobile.scss";
import { connect } from "react-redux";
import { BallBeat } from 'react-pure-loaders';
import pengusaha from "../../assets/img/home/html/pengusahailustrasi.svg"
import pegawai from "../../assets/img/home/html/pekerjailustrasi.svg"
import berita1 from "../../assets/img/home/thought-catalog-UK78i6vK3sc-unsplash@2x.png"
import berita2 from "../../assets/img/home/glenn-carstens-peters-SL5d_8ywAAA-unsplash@2x.png"
import berita3 from "../../assets/img/home/dayne-topkin-y5_mFlLMwJk-unsplash@2x.png"
import Navbar from "../../components/navbar_home";
import Footer from "../../components/Footer";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Carousel from 'react-bootstrap/Carousel'
import slideimg from '../../assets/img/home/smartphone_mockup@2x.png';
import $ from 'jquery'

import { Divider, Image, Icon } from 'semantic-ui-react'

class Home extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoading: true,
      data:[]
    };
  }
  
  componentDidMount() {
    this.setState({isLoading:true});
}
  
	render() {
    
    $("#daftarSekarang").click(function() {
        $('html, body').animate({
            scrollTop: $("#ChooseDaftar").offset().top
        }, 2000);
    });

		return(
      <div className='bghead'>
          <BallBeat
          color={'#123abc'}
          loading={this.isLoading}
          />
          <Navbar/>
          <Header/>
          <TentangApp/>
          <Pendaftaran/>
          <SliderApp />
          <Berita/>
          <Footer/>
      </div>
		);
	}
  
}

function Header() {
  return (
    <Container>
    <Row className="justify-content-md-center">
        <Col xs="12" md="6" className='headtopp'>
          <div className='detailheadtopp'>
            AppOn.id adalah marketplace jasa terintegrasi untuk UMKM dan <br/>Pekerja Remote
          </div>
          <div className='subheadtopp'>
            AppOn memfasilitasi UKM memenuhi kebutuhan bisnis, terhubung dengan layanan jasa dan pekerja remot yang siap mengembangkan UKM 
            menjadi juara dan mengkonversi traffic menjadi cuan!
          </div>
          <a href="#" id="daftarSekarang" className="btn btn-danger daftarBtn">Daftar Sekarang</a>
        </Col>
        <Col xs="12" md="6" className='bgtop'></Col>
    </Row>
    </Container>
  );
}


function TentangApp() {
  return (
    <Container>
    <Row className="justify-content-md-center">
        <Col xs="12" md="12" className='tentangapp'>
            <span className="headtentang">Tentang AppOn</span>
            <p className="childTentang">Tumbuh berkembang bersama kami</p>
            <div className="isitentangappon">
              <p className='p1'>
                <strong>AppOn</strong> merupakan satu-satunya Platform aplikasi kerja remote terintegrasi yang berfungsi sebagai penghubung antara profesi 
                pekerja konten spesialis di seluruh Indonesia dengan perusahaan yang mencari kandidat freelance konten spesialis yang 
                sesuai dengan kebutuhan dan kemampuannya melalui sistem kerja remote (kerja jarak jauh), dengan menggunakan teknologi 
                data scraper yang dikembangkan khusus untuk mengatur, memonitor segala data kegiatan antara konten spesialis dengan 
                perusahaan yang telah melakukan kesepakatan kerjasama tentunya itu menjadi nilai lebih <strong>AppOn</strong> sebagai mitra terpercaya anda.
              </p>
              <p className='p1'>
                Saat ini hanya <strong>AppOn</strong> yang dapat menjadi jawaban para pencari kerja untuk mendapatkan pekerjaan yang terhindar dari kemacetan, 
                tertular virus, dan menghabiskan waktu di jalan. <strong>AppOn</strong> memiliki misi untuk meningkatkan kualitas pekerja remote di Indonesia. 
                Kami terus melakukan penelitian untuk mengoptimasi pekerjaan remote hingga dapat menjadi metode kerja yang dapat memenuhi 
                target KPI pekerjaan secara jelas dan termonitor. Hanya di aplikasi <strong>AppOn</strong> anda dapat mengejar 
                karir profesi anda hingga menjadi seorang konsultan Brand atau Influencer ternama. 
              </p>
              <p className='olehpt'>
                AppOn dikembangkan oleh PT. Armada Entitas Online (AREON) Bandung Indonesia.
              </p>
            </div>
        </Col>
    </Row>
    </Container>
  );
}

function Pendaftaran() {
  return (
    <Container style={{paddingTop:'50px'}}>
    <Row className="justify-content-md-center">
        <Col xs="12" md="12" className='pendaftaranapp'>
            <span id="ChooseDaftar" className="headpendaftaran">Pendaftaran untuk pengusaha maupun pekerja</span>
            <p className="pendaftaranTentang">Daftarkan usaha dan keahlian anda di AppOn, dan tingkatkan target anda. 
            <br/>Daftarkan diri anda sebagai:</p>
            <Row className="justify-content-md-center" style={{paddingTop:'30px'}}>
              <Col xs="12" md="6" className='justify-content-md-right'>
                <div className='pengusahaCard pengusahaRight'> 
                    <p className='judulpengusaha'>Pengusaha</p>
                    <Image src={pengusaha} className='inline'/>
                    <p className='subjudulpendaftaran'>Daftar sebagai pengusaha untuk menawarkan pekerjaan dan mencari pekerja</p>
                    <a href={`/Signup/?as=Pengusaha`} className="btn btn-danger daftarBtnPendaftaran">DAFTAR</a>
                </div>
              </Col>
              <Col xs="12" md="6">
                <div className='pengusahaCard' style={{marginLeft:'50px'}}> 
                  <p className='judulpengusaha'>Pekerja</p>
                  <Image src={pegawai} className='inline' /> 
                  <p className='subjudulpendaftaran'>Daftar sebagai pekerja dan cari <br/> pekerjaan sesuai dengan <br/> keahlianmu</p>
                  <a href={`/Signup/?as=Pekerja`}  className="btn btn-danger daftarBtnPendaftaran">DAFTAR</a>
                </div>
              </Col>
            </Row>
        </Col>
    </Row>
    </Container>
  );
}

function SliderApp() {
  return (
    <div className='paddslider' style={{paddingTop:'50px'}}>
      <Carousel>
        <Carousel.Item>
          <Row className="justify-content-md-center">
              <Col xs="12" md="12" className=''>
                  <Row className="justify-content-md-center contentpendaftaran" >
                    <Col xs="12" md="6" className='bgslidapp'></Col>
                    <Col xs="12" md="6" className='bgsliddark'>
                      <p className='judulsliddark'>AppOn.id Untuk UKM</p>
                      <p className='subsliddark'>Temukan berbagai layanan untuk menembangkan bisnismu dengan
                      <br/> mudah, tanpa perlu keluar rumah, 
                      tanpa perlu ruang kantor, tanpa perlu <br/> ribet mikirin gaji karyawan, semua bisa kamu dapatkan semudah <br/> berbelanja online.
                      </p>
                    </Col>
                  </Row>
              </Col>
          </Row>
        </Carousel.Item>
        <Carousel.Item>
          <Row className="justify-content-md-center">
              <Col xs="12" md="12" className=''>
                  <Row className="justify-content-md-center contentpendaftaran">
                    <Col xs="12" md="6" className='bgslidapp'></Col>
                    <Col xs="12" md="6" className='bgsliddark'>
                      <p className='judulsliddark'>AppOn.id Untuk Pekerja</p>
                      <p className='subsliddark'>
                        Kini kamu bisa mendapatkan penghasilan darimanapun dan <br/> kapanpun, dengan kerja secara remote kamu bisa lebih produktif <br/> 
                        dan leluasa mengatur waktu. Serunya lagi, disini kamu bisa punya <br/>karir yang berjenjeng.
                      </p>
                    </Col>
                  </Row>
              </Col>
          </Row>
        </Carousel.Item>
        <Carousel.Item>
          <Row className="justify-content-md-center">
              <Col xs="12" md="12" className=''>
                  <Row className="justify-content-md-center contentpendaftaran">
                    <Col xs="12" md="6" className='bgslidapp'></Col>
                    <Col xs="12" md="6" className='bgsliddark'>
                      <p className='judulsliddark'>AppOn.id Solusi Cerdas untuk kamu yang #dirumahaja</p>
                      <p className='subsliddark'>
                        Sempat terpikir gak gimana caranya bisa #dirumahaja <br/> tapi dapat pengasilan? 
                        Atau kamu punya bisnis tapi bingung <br/> kembanginnya disaat seperti ini? Cobain AppOn.id! 
                        <br/> Disini kamu temukan solusi untuk tetap produktif <br/> selama #dirumahaja.
                      </p>
                    </Col>
                  </Row>
              </Col>
          </Row>
        </Carousel.Item>
    </Carousel>
    </div>
  );
}

function Berita() {
  return (
    <Container>
    <Row className="justify-content-md-center">
        <Col xs="12" md="12" className='beritaapp'>
            <span className="headberita">Berita seputar dunia kerja & usaha</span>
            <p className="beritatentang">Temukan berita, tips, trik dan saran dari AppOn</p>
            <Row className="justify-content-md-center mberitaRow">
              <Col xs="12" md="3" className="cardberita mberita1" style={{background:'url('+berita1+')',
              backgroundSize:'100%',backgroundRepeat:'no-repeat',
              paddingLeft:'0px',backgroundColor:'#E9E9E9'}}>
                <div className='bgjudulberita'>
                  <p className='judulberita'>Kerja Remote: Mulailah Dari Hal yang Paling Sederhana</p>
                </div>
                <Row className="justify-content-md-center midberita">
                  <Col xs="6" md="6">
                    <Icon name='user outline' />
                    Admin
                  </Col>
                  <Col xs="6" md="6">
                    <Icon name='clock outline' />
                    Kamis, 10 April 2020
                  </Col>
                </Row>
                <div className='padjudulberita'>
                  <p className='descjudulberita'>
                    Kerja Remote: Mulailah Dari Hal yang Paling Sederhana.
                    Bekerja dari jarak jauh atau mengurangi aktivitas pertemuan fisik, sebenarnya bukan hanya terjadi pada saat…
                  </p>
                </div>
                <a href="#" className="btn BtnSelengkapnya">Baca Selengkapnya</a>
              </Col>
              <Col xs="12" md="3" className="cardberita mberita2" style={{background:'url('+berita2+')',
              backgroundSize:'100%',backgroundRepeat:'no-repeat',
              paddingLeft:'0px',backgroundColor:'#E9E9E9'}}>
                <div className='bgjudulberita'>
                  <p className='judulberita'>Kerja Remote: Mulailah Dari Hal yang Paling Sederhana</p>
                </div>
                <Row className="justify-content-md-center midberita">
                  <Col xs="6" md="6">
                    <Icon name='user outline' />
                    Admin
                  </Col>
                  <Col xs="6" md="6">
                    <Icon name='clock outline' />
                    Kamis, 10 April 2020
                  </Col>
                </Row>
                <div className='padjudulberita'>
                  <p className='descjudulberita'>
                    Kerja Remote: Mulailah Dari Hal yang Paling Sederhana.
                    Bekerja dari jarak jauh atau mengurangi aktivitas pertemuan fisik, sebenarnya bukan hanya terjadi pada saat…
                  </p>
                </div>
                <a href="#" className="btn BtnSelengkapnya">Baca Selengkapnya</a>
              </Col>
              <Col xs="12" md="3" className="cardberita mberita3" style={{background:'url('+berita3+')',
              backgroundSize:'100%',backgroundRepeat:'no-repeat',
              paddingLeft:'0px',backgroundColor:'#E9E9E9'}}>
                <div className='bgjudulberita'>
                  <p className='judulberita'>Kerja Remote: Mulailah Dari Hal yang Paling Sederhana</p>
                </div>
                <Row className="justify-content-md-center midberita">
                  <Col xs="6" md="6">
                    <Icon name='user outline' />
                    Admin
                  </Col>
                  <Col xs="6" md="6">
                    <Icon name='clock outline' />
                    Kamis, 10 April 2020
                  </Col>
                </Row>
                <div className='padjudulberita'>
                  <p className='descjudulberita'>
                    Kerja Remote: Mulailah Dari Hal yang Paling Sederhana.
                    Bekerja dari jarak jauh atau mengurangi aktivitas pertemuan fisik, sebenarnya bukan hanya terjadi pada saat…
                  </p>
                </div>
                <a href="#" className="btn BtnSelengkapnya">Baca Selengkapnya</a>
              </Col>
            </Row>
            <Row className="justify-content-md-center" style={{paddingTop:'50px'}}>
              <a href="#" className="btn BtnArtikel">Lihat Semua Artikel <i aria-hidden="true" className="arrow right red icon"></i> </a>
            </Row>
        </Col>
    </Row>
    </Container>
  );
}

function mapStateToProps(state) {
  return { loginUser: state.authReducer.loginUser }
}

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
