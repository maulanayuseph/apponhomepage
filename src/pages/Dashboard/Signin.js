import PropTypes from 'prop-types'
import React, { Component, memo, useState  } from 'react'
import {Modal} from 'react-bootstrap';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import '../../App.scss'
import '../../AppMobile.scss'
import Navbar from "../../components/navbar";
import googleicon from "../../assets/img/home/html/googleicon.svg"
import fbicon from "../../assets/img/home/html/facebookicon.svg"
import {
  Button,
  Form,
  Checkbox,
  Grid,
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Input,
  Label,
} from 'semantic-ui-react'


export default class Signin extends Component {
  state = {
    animation: 'push',
  }
  
  render() {
    return (
        <div className='bgheadSignin'>
          <Navbar/>
          <FormSignin />
      </div>
    )
  }

}


function FormSignin() {
    return (
      <Container>
      <Row className="justify-content-md-center">
          <Col xs="12" md="6" className='headtopp2'>
            <div className='detailheadtoppSign'>
            <Form>
              <Form.Field>
                <label>Email</label>
                <input placeholder='Email' />
              </Form.Field>
              <Form.Field>
                <label>Password</label>
                <input placeholder='Password' />
              </Form.Field>
              <Form.Field className='row'>
                {/* <Button type='submit' className='btn daftarBtnSignin colorSign'>MASUK</Button> */}
                <a href='/FormKebutuhanPengusaha' className='btn daftarBtnSignin colorSign'>MASUK</a>
              </Form.Field>
            </Form>
            </div>
            <div className='subheadtoppSign'>
              Atau masuk dengan
              <Image.Group size='tiny'>
                  <a href="#" onClick={(e) => {
                    e.preventDefault()
                  }} style={{paddingRight:'30px',textDecoration:'none'}}>
                    {/* <i aria-hidden="true" className="google red big icon"></i> */}
                    <Image src={googleicon} />
                  </a>
                  <a href="#" onClick={(e) => {
                    e.preventDefault()
                  }}>
                    <Image src={fbicon} />
                    {/* <i aria-hidden="true" className="facebook official red big icon"></i> */}
                  </a>
                </Image.Group>
            </div>
          </Col>
          <Col xs="12" md="6" className='paddingDaftar'>
            <div className='textSigninDaftar'>
                Anda belum mempunyai akun?
            </div>
            <Daftar/>
          </Col>
      </Row>
      </Container>
    )
}

function Daftar() {
  const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

  return (
    <>
      <a href="#" onClick={handleShow} className="btn daftarBtnSignup">DAFTAR</a>
      <Modal centered show={show} onHide={handleClose} animation={false}>
          <Modal.Title className="titleModalDaftar">Daftar sebagai</Modal.Title>
       
          <div className="row text-center contentModalDaftar">
            <div className="col-md-6">
              <a href="/Signup?as=Pengusaha" className="btn my-4 btnDaftarPengusaha">PENGUSAHA</a>
            </div>
            <div className="col-md-6">
            <a href="/Signup?as=Pekerja" className="btn my-4 btnDaftarPekerja">PEKERJA</a>
            </div>
          </div>
      
      </Modal>
    </>
  );
}