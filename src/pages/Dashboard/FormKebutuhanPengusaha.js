import PropTypes from 'prop-types'
import React, { Component, memo, useState  } from 'react'
import {Modal} from 'react-bootstrap';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import '../../App.scss'
import '../../AppMobile.scss'
import Navbar from "../../components/navbar_dashboard";
import googleicon from "../../assets/img/home/html/googleicon.svg"
import fbicon from "../../assets/img/home/html/facebookicon.svg"
import delIcon from "../../assets/img/home/html/Mask_Group_224.png"
import bgilustrasi from "../../assets/img/home/bgilustrasi.svg"

import $ from "jquery"
import {
  Button,
  Form,Dropdown,Radio,
  Checkbox,
  Grid,
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Input,
  TextArea,
  Label,
} from 'semantic-ui-react'


export default class FormKebutuhanPengusaha extends Component {
  state = {
    animation: 'push',
    activeItem:'PROFILE',
    users:[],
  }
  

  render() {
    const handleItemClick = (e, { name }) => {
        this.setState({ activeItem: name })
        if (name === 'PROFILE') {
            $("#PROFILE").attr('style','display');
        } else {
            $("#PROFILE").attr('style','display:none');
        }
    }

    const handleChange = (e, { value }) => this.setState({ value })

    const addUser = (e) => {
        this.setState({
            users: [...this.state.users, <User addUser={this.state.users} />]
        })
    }

    return (
        <div className='bgheadDashboard'>
          <Navbar/>
          <HeadBar handleChange={handleChange} activeItem={this.state.activeItem} handleItemClick={handleItemClick}/>
      </div>
    )
  }

}

const User = memo((props) => {
    return (
        <Form.Field>
            <Input
                action={
                    <Button className='btnUploadFile' type="file" style={{color:"#FFFFFF"}}>Upload</Button>
                }
                className="addKeahlian"
                icon='attach'
                iconPosition='left'
                placeholder='File001.jpeg'
            />
            <Button className='imgdelfileKeahlian' icon>
                <Image src={delIcon} size="mini" className="delfileKeahlian"/>
            </Button>
        </Form.Field>
        )
});

const HeadBar = memo((props) => {
    return (
        <Row className="justify-content-md-center ">
            <Col xs="12" md="12">
            <Segment className='menuHeadbar' inverted>
                <Menu inverted pointing secondary>
                    <Menu.Item
                    name='PROFILE'
                    active={props.activeItem === 'PROFILE'}
                    onClick={props.handleItemClick}
                    />
                    <Menu.Item
                    name='DASHBOARD'
                    />
                    <Menu.Item
                    name='INBOX'
                    />
                </Menu>
            </Segment>
            <Konten addUser={props.addUser} users={props.users} handleChange={props.handleChange}/>
            </Col>
        </Row>
    );
});

const Konten = memo((props) => {
    const onClickPrev = (e) => {
        e.preventDefault()
        props.addUser()
    }
      
    return (
        <Container>
        <Row className="justify-content-md-center">
          <Col xs="12" md="12" className='kontenKebPekerja'>
            <Grid stackable columns={2}>
                <Grid.Row>
                <Grid.Column>
                    <span className='judulkontenKebPekerja'>Silahkan lengkapi data berikut agar kami dapat 
                    <br/>mengetahui dan membantu sesuai kebutuhan anda</span>
                    <Form className='formKebPekerja'>
                        <Form.Field>
                            <label>Nama</label>
                            <input placeholder='Nama' />
                        </Form.Field>
                        <Form.Field>
                            <label>Email</label>
                            <input placeholder='Email' />
                        </Form.Field>
                        <Form.Field>
                            <label>Nama Usaha</label>
                            <input placeholder='Nama Usaha' />
                        </Form.Field>
                        <Form.Field>
                            <label>Jenis Usaha</label>
                            <input placeholder='Nama Usaha' />
                        </Form.Field>
                        <Form.Field>
                            <label>Kebutuhan anda saat ini :</label>
                        </Form.Field>
                        <Form.Field>
                            <Radio
                                label='Toko online'
                                name='radioGroup'
                                value='this'
                                className='radioText'
                                onChange={props.handleChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Radio
                                label='Admin online'
                                name='radioGroup'
                                value='this'
                                className='radioText'
                                onChange={props.handleChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Radio
                                label='Pengelolaan konten sosmed'
                                name='radioGroup'
                                value='this'
                                className='radioText'
                                onChange={props.handleChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Radio
                                label='Desain kemasan'
                                name='radioGroup'
                                value='this'
                                className='radioText'
                                onChange={props.handleChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Radio
                                label='Marketing atau iklan online'
                                name='radioGroup'
                                value='this'
                                className='radioText'
                                onChange={props.handleChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <Radio
                                label='Lainnya'
                                name='radioGroup'
                                value='this'
                                className='radioText'
                                onChange={props.handleChange}
                            />
                        </Form.Field>
                        <Form.Field>
                            <input placeholder='Lainnya' />
                        </Form.Field>
                        <Form.Field className='row'>
                            <a href='/FormKebutuhanPekerja' className='btn simpanKeb colorSign'>SIMPAN</a>
                        </Form.Field>
                    </Form>
                </Grid.Column>
                </Grid.Row>
            </Grid>
          </Col>
      </Row>
      </Container>
    )
})