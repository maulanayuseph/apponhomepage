import PropTypes from 'prop-types'
import React, { Component, memo, useState  } from 'react'
import {Modal} from 'react-bootstrap';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import '../../App.scss'
import '../../AppMobile.scss'
import Navbar from "../../components/navbar_onstoresub";
import googleicon from "../../assets/img/home/html/googleicon.svg"
import fbicon from "../../assets/img/home/html/facebookicon.svg"
import bgtesti from '../../assets/img/home/html/onstore/Path 647.svg';
import nomor1 from '../../assets/img/home/html/onstore/Group 15268.png';
import nomor2 from '../../assets/img/home/html/onstore/Group 15266 line.png';
import nomor3 from '../../assets/img/home/html/onstore/Group 15267 line.png';
import nomor4 from '../../assets/img/home/html/onstore/Group 15269 line.png';
import {
  Button,
  Form,
  Checkbox,
  Grid,
  Header,
  Icon,
  Image,
  Menu,
  Segment,
  Sidebar,
  Input,
  Label,
} from 'semantic-ui-react'


export default class Pilihsubdomain extends Component {
  state = {
    animation: 'push',
  }
  
  render() {
    return (
        <div className='bgheadstore subdomain'>
          <Navbar/>
          <HeaderKonten/>
      </div>
    )
  }
  
}

function HeaderKonten() {
    return (
        <Container>
        <Row className="justify-content-md-left">
            <Col xs="12" md="6" className='headtoppsubdomain'>
            <Header as='h2' className="subdomain mlangkah1">
                    <Image src={nomor1}/>
                    <Header.Content>
                    <span className="headstep">Langkah 1</span>
                    <Header.Subheader><span className="subheadstep">Pilih Nama Toko</span></Header.Subheader>
                </Header.Content>
            </Header>

            <Header as='h2' className="paddDomain subdomain mlangkah2">
                    <Image src={nomor2} className="notactive"/>
                    <Header.Content>
                    <span className="headstepnotactive notactive">Langkah 2</span>
                    <Header.Subheader><span className="subheadstepnotactive notactive">Isi data diri</span></Header.Subheader>
                </Header.Content>
            </Header>

            <Header as='h2' className="paddDomain subdomain mlangkah3">
                    <Image src={nomor3} className="notactive"/>
                    <Header.Content>
                    <span className="headstepnotactive notactive">Langkah 3</span>
                    <Header.Subheader><span className="subheadstepnotactive notactive">Masukan kode promosi</span></Header.Subheader>
                </Header.Content>
            </Header>

            <Header as='h2' className="paddDomain subdomain mlangkah4">
                    <Image src={nomor4} className="notactive"/>
                    <Header.Content>
                    <span className="headstepnotactive notactive">Langkah 4</span>
                    <Header.Subheader><span className="subheadstepnotactive notactive">Metode pembayaran</span></Header.Subheader>
                </Header.Content>
            </Header>

            </Col>
            <Col xs="12" md="6" className='formheadtoppsubdomain'>
                <div className="formNamaToko">
                <Form>
                    <Form.Field>
                        <label>Masukkan Nama Toko</label>
                        <input placeholder='Masukkan Nama Toko' />
                        <span className="subnamatoko">Masukkan nama tanpa spasi</span>
                    </Form.Field>
                    <Button type='submit' className="btnCekToko">Cek Nama Toko</Button>
                </Form>
                </div>
                <div className="footerNext">
                    <Button type='button' className="btnNextToko" href="/Isiform">
                        Selanjutnya
                        <Icon name='right arrow' className="warnaRed" />
                    </Button>
                </div>
            </Col>
        </Row>
        </Container>
    )
}
